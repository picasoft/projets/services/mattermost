#!/bin/bash

export_sql_request_tmpl="""
SELECT u.id as userid
FROM users u
LEFT JOIN (
  SELECT userid, MAX(createat) as lastlogin 
  FROM audits a
  WHERE a.action = '/api/v4/users/login' AND a.extrainfo LIKE 'success%' 
  GROUP BY userid
) lastlogin
ON 
   u.id = lastlogin.userid
LEFT JOIN (
  SELECT userid, MAX(lastactivityat) AS lastactive
  FROM sessions s
  GROUP BY userid
) lastactive
ON
   u.id = lastactive.userid
WHERE
   TO_TIMESTAMP(GREATEST(lastlogin.lastlogin, lastactive.lastactive)/1000) < TO_TIMESTAMP(ALERT_DATE_MIDNIGHT_EPOCH) - INTERVAL '3 years'
   AND u.deleteat = 0;
"""

alert_date_midnight_epoch=$(date -d "today - 1 month 00:00:00" +'%s')
inactive_users=$(echo "$export_sql_request_tmpl" | sed "s/ALERT_DATE_MIDNIGHT_EPOCH/$alert_date_midnight_epoch/" | psql -h mattermost-db -qAtX -U mattermost)

for userid in $(echo $inactive_users)
do
	curl -X DELETE -H "Authorization: Bearer $MM_ADMIN_TOKEN" "https://team.picasoft.net/api/v4/users/$userid"
done
