#!/bin/bash

if [ -z "$SMTP_PASSWORD" ]
then
	echo "missing SMTP_PASSWORD"
	exit 1
fi

if [ -z "$MM_DB_PASSWORD" ]
then
	echo "missing MM_DB_PASSWORD"
	exit 1
fi

if [ -z "$MM_ADMIN_TOKEN" ]
then
	echo "missing MM_ADMIN_TOKEN"
	exit 1
fi

echo "mattermost-db:5432:mattermost:mattermost:$MM_DB_PASSWORD" > /root/.pgpass
chmod 0600 /root/.pgpass

exec "$@"
