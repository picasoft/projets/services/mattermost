#!/bin/bash

export_sql_request_tmpl="""
SELECT json_agg(exp) FROM (
SELECT
   u.id as userid,
   u.username as username,
   u.email as email,
   TO_TIMESTAMP(GREATEST(lastlogin.lastlogin, lastactive.lastactive)/1000) as last_login_date,
   userteams.nameteams as teams
FROM users u
LEFT JOIN (
  SELECT userid, MAX(createat) as lastlogin 
  FROM audits a
  WHERE a.action = '/api/v4/users/login' AND a.extrainfo LIKE 'success%' 
  GROUP BY userid
) lastlogin
ON 
   u.id = lastlogin.userid
LEFT JOIN (
  SELECT userid, MAX(lastactivityat) AS lastactive
  FROM sessions s
  GROUP BY userid
) lastactive
ON
   u.id = lastactive.userid
LEFT JOIN (
  SELECT teammembers.userid as userid, array_agg(teams.displayname) as nameteams
  FROM teammembers
  INNER JOIN teams ON teammembers.teamid = teams.id
  WHERE teammembers.deleteat = 0
  GROUP BY teammembers.userid
) userteams
ON
   u.id = userteams.userid
WHERE
   TO_TIMESTAMP(GREATEST(lastlogin.lastlogin, lastactive.lastactive)/1000) < TO_TIMESTAMP(TODAY_MIDNIGHT_EPOCH) - INTERVAL '3 years'
   AND u.deleteat = 0
) exp;
"""
today_midnight_epoch=$(date +'%s' -d 'today 00:00:00')

inactive_accounts=$(echo "$export_sql_request_tmpl" | sed "s/TODAY_MIDNIGHT_EPOCH/$today_midnight_epoch/" | psql -h mattermost-db -qAtX -U mattermost)
if [ $? -ne 0 ]
then
	echo "Failed to request inactive accounts"
	exit 1
fi

echo $inactive_accounts | jq -c '.[]' | while read i
do
	email_body=$(echo $i | gomplate -c ".=stdin:///data.yaml" -f email_inactive_account.tmpl)
	if [ $? -ne 0 ]
	then
		exit 1
	fi
	rcpt_to=$(echo $i | jq -r .email)
	if [ $? -ne 0 ]
	then
		exit 1
	fi
	if [ -z "$email_body" ]
	then
		echo "email body is empty for $rcpt_to"
		continue
	fi
	if [ -z "$rcpt_to" ]
	then
		echo "email rcpt to is empty"
		continue
	fi
	echo "$email_body" | curl -v --mail-from mattermost@picasoft.net --ssl --mail-rcpt "$rcpt_to" --user "mattermost:$SMTP_PASSWORD" --upload-file - smtp://mail.picasoft.net:587/team.picasoft.net
	if [ $? -ne 0 ]
	then
		echo "Failed to send email to $rcpt_to"
	fi
done
