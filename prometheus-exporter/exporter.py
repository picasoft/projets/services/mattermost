#!/usr/bin/env python3

"""Prometheus exporter for Mattermost"""

# Imports
import os
import sys
import time
from datetime import datetime
import signal
from prometheus_client import start_http_server, Gauge, REGISTRY, PROCESS_COLLECTOR, PLATFORM_COLLECTOR
import psycopg2

# Mattermost channels types
MM_CHANNEL_PUBLIC = "O"
MM_CHANNEL_PRIVATE = "P"
MM_CHANNEL_DIRECT = "D"
MM_CHANNEL_GROUP = "G"


def db_connect():
    """
    Connect to Mattermost database.
    :returns: psycopg2 connector
    """
    # Get database credentials
    host = os.getenv("EXPORTER_DB_HOST", "mattermost-db")
    port = int(os.getenv("EXPORTER_DB_PORT", "5432"))
    dbname = os.getenv("EXPORTER_DB_NAME", "mattermost")
    user = os.getenv("EXPORTER_DB_USER", "mattermost")
    password = os.getenv("EXPORTER_DB_PASSWORD")
    if password is None:
        print("EXPORTER_DB_PASSWORD must be set.")
        return None

    # Connect to an existing database
    conn = psycopg2.connect(host=host, port=port, dbname=dbname, user=user, password=password)

    return conn


def get_users_count(db_conn):
    """
    Get the number of active and deleted users
    :param db_conn: Mattermost database connector
    :returns: Dict
    {
        "active": 160,
        "deleted": 12
    }
    """
    data = {}

    # Create database cursor
    db_cursor = db_conn.cursor()

    # Query for active users
    db_cursor.execute(
        "SELECT COUNT(DISTINCT u.id) FROM users AS u LEFT JOIN Bots AS b ON u.id = b.userid WHERE u.deleteat = 0 AND b.userid IS NULL;"
    )
    data['active'] = db_cursor.fetchone()[0]

    # Query for delete users
    db_cursor.execute(
        "SELECT COUNT(DISTINCT u.id) FROM users AS u LEFT JOIN Bots AS b ON u.id = b.userid WHERE u.deleteat != 0 AND b.userid IS NULL;"
    )
    data['deleted'] = db_cursor.fetchone()[0]

    # Close cursor
    db_cursor.close()
    return data


def get_bots_count(db_conn):
    """
    Get the number of active and deleted bots
    :param db_conn: Mattermost database connector
    :returns: Dict
    {
        "active": 160,
        "deleted": 12
    }
    """
    data = {}

    # Create database cursor
    db_cursor = db_conn.cursor()

    # Query for active bots
    db_cursor.execute("SELECT COUNT(userid) FROM Bots WHERE deleteat = 0;")
    data['active'] = db_cursor.fetchone()[0]

    # Query for delete bots
    db_cursor.execute("SELECT COUNT(userid) FROM Bots WHERE deleteat != 0;")
    data['deleted'] = db_cursor.fetchone()[0]

    # Close cursor
    db_cursor.close()
    return data


def get_posts_count(db_conn):
    """
    Get the number of posts
    :param db_conn: Mattermost database connector
    :returns: Count of posts
    """
    # Create database cursor
    db_cursor = db_conn.cursor()
    # Query for user count
    db_cursor.execute("SELECT COUNT(id) FROM Posts;")
    posts_count = db_cursor.fetchone()[0]
    # Close cursor
    db_cursor.close()
    return posts_count


def get_teams_count(db_conn):
    """
    Get the number of teams for each type
    :param db_conn: Mattermost database connector
    :returns: Dict of teams count by type
    {
        "public": 12,
        "private": 4,
        "deleted": 1
    }
    """
    data = {}
    # Create database cursor
    db_cursor = db_conn.cursor()

    # Query for public teams
    db_cursor.execute("SELECT COUNT(id) FROM Teams WHERE allowopeninvite = true AND deleteat = 0;")
    data["public"] = db_cursor.fetchone()[0]
    # Query for private teams
    db_cursor.execute("SELECT COUNT(id) FROM Teams WHERE allowopeninvite = false AND deleteat = 0;")
    data["private"] = db_cursor.fetchone()[0]
    # Query for deleted teams
    db_cursor.execute("SELECT COUNT(id) FROM Teams WHERE deleteat != 0;")
    data["deleted"] = db_cursor.fetchone()[0]

    # Close cursor
    db_cursor.close()
    return data


def get_channels_count(db_conn):
    """
    Get the number of channels for each type
    :param db_conn: Mattermost database connector
    :returns: Dict of channels count by type
    {
        "public": 12,
        "private": 4,
        "direct": 8,
        "group": 1
    }
    """
    data = {}
    # Create database cursor
    db_cursor = db_conn.cursor()

    # Query for public channels count
    db_cursor.execute("SELECT COUNT(id) FROM Channels WHERE type = '"+MM_CHANNEL_PUBLIC+"';")
    data["public"] = db_cursor.fetchone()[0]
    # Query for private channels count
    db_cursor.execute("SELECT COUNT(id) FROM Channels WHERE type = '"+MM_CHANNEL_PRIVATE+"';")
    data["private"] = db_cursor.fetchone()[0]
    # Query for direct message channel count
    db_cursor.execute("SELECT COUNT(id) FROM Channels WHERE type = '"+MM_CHANNEL_DIRECT+"';")
    data["direct"] = db_cursor.fetchone()[0]
    # Query for group message channels count
    db_cursor.execute("SELECT COUNT(id) FROM Channels WHERE type = '"+MM_CHANNEL_GROUP+"';")
    data["group"] = db_cursor.fetchone()[0]

    # Close cursor
    db_cursor.close()
    return data


def get_daily_posts_count(db_conn):
    """
    Get the number of posts on the current day
    :param db_conn: Mattermost database connector
    :returns: Count of posts for today
    """
    # Create database cursor
    db_cursor = db_conn.cursor()

    # Get today start and end timestamps
    today_start = int(datetime.now().replace(hour=0, minute=0, second=0, microsecond=0).timestamp() * 1000)
    today_end = int(datetime.now().replace(hour=23, minute=59, second=59, microsecond=999999).timestamp() * 1000)

    # Query for daily posts
    db_cursor.execute(
        "SELECT Count(Posts.Id) AS Value FROM Posts WHERE Posts.CreateAt <= " + str(today_end) +
        " AND Posts.CreateAt >= " + str(today_start) + " GROUP BY DATE(TO_TIMESTAMP(Posts.CreateAt / 1000));"
    )
    daily_posts_count = db_cursor.fetchone()[0]
    # Close cursor
    db_cursor.close()
    return daily_posts_count


def get_daily_users_count(db_conn):
    """
    Get the number of users who wrote a message on the current day
    :param db_conn: Mattermost database connector
    :returns: Count of users for today
    """
    # Create database cursor
    db_cursor = db_conn.cursor()

    # Get today start and end timestamps
    today_start = int(datetime.now().replace(hour=0, minute=0, second=0, microsecond=0).timestamp() * 1000)
    today_end = int(datetime.now().replace(hour=23, minute=59, second=59, microsecond=999999).timestamp() * 1000)

    # Query for daily users
    db_cursor.execute(
        "SELECT COUNT(DISTINCT Posts.UserId) FROM Posts WHERE Posts.CreateAt <= " +
        str(today_end) + " AND Posts.CreateAt >= " + str(today_start) + ";"
    )
    daily_users_count = db_cursor.fetchone()[0]
    # Close cursor
    db_cursor.close()
    return daily_users_count


def main():
    """Main function"""
    # Number of seconds between 2 metrics collection
    collect_interval = int(os.getenv('EXPORTER_COLLECT_INTERVAL', "60"))
    # Port for metrics server
    exporter_port = 8000
    # Get instance name for metrics tag
    instance_name = os.getenv('INSTANCE_NAME')
    if instance_name is None:
        print("INSTANCE_NAME must be set.")
        return None

    # Connect to Mattermost database
    db_conn = db_connect()
    if db_conn is None:
        print("Cannot connect to Mattermost database.")
        sys.exit(-1)

    # Define an exit function to close connection
    def exit_handler(sig, frame):
        print('Terminating...')
        # Close database connection
        db_conn.close()
        sys.exit(0)

    # Catch SIGINT and SIGTERM signals
    signal.signal(signal.SIGINT, exit_handler)
    signal.signal(signal.SIGTERM, exit_handler)

    # Remove unwanted Prometheus metrics
    [REGISTRY.unregister(c) for c in [
        PROCESS_COLLECTOR,
        PLATFORM_COLLECTOR,
        REGISTRY._names_to_collectors['python_gc_objects_collected_total']
    ]]

    # Start Prometheus exporter server
    start_http_server(exporter_port)

    # Register metrics
    users_gauge = Gauge('mattermost_users_total', 'Number of regular users', ['instance_name', 'state'])
    bots_gauge = Gauge('mattermost_bots_total', 'Number of bots users', ['instance_name', 'state'])
    channels_gauge = Gauge('mattermost_channels_total', 'Number of channels', ['instance_name', 'type'])
    posts_gauge = Gauge('mattermost_posts_total', 'Number of posts', ['instance_name'])
    teams_gauge = Gauge('mattermost_teams_total', 'Number of teams', ['instance_name', 'type'])
    daily_posts_gauge = Gauge('mattermost_daily_posts_total', 'Number of posts on current day', ['instance_name'])
    daily_users_gauge = Gauge(
        'mattermost_daily_users_total',
        'Number of active users on current day',
        ['instance_name']
    )

    # Loop forever
    while True:
        # Update users
        users = get_users_count(db_conn)
        for user_state in users:
            users_gauge.labels(instance_name=instance_name, state=user_state).set(users[user_state])

        # Update bots users
        bots = get_bots_count(db_conn)
        for bot_state in bots:
            bots_gauge.labels(instance_name=instance_name, state=bot_state).set(bots[bot_state])

        # Update channels
        channels = get_channels_count(db_conn)
        for chan_type in channels:
            channels_gauge.labels(instance_name=instance_name, type=chan_type).set(channels[chan_type])

        # Update posts
        posts_count = get_posts_count(db_conn)
        posts_gauge.labels(instance_name=instance_name).set(posts_count)

        # Update teams
        teams = get_teams_count(db_conn)
        for team_type in teams:
            teams_gauge.labels(instance_name=instance_name, type=team_type).set(teams[team_type])

        # Update daily posts
        daily_posts_count = get_daily_posts_count(db_conn)
        daily_posts_gauge.labels(instance_name=instance_name).set(daily_posts_count)

        # Update daily users
        daily_users_count = get_daily_users_count(db_conn)
        daily_users_gauge.labels(instance_name=instance_name).set(daily_users_count)

        # Wait before next metrics collection
        time.sleep(collect_interval)


if __name__ == '__main__':
    main()
