networks:
  proxy:
    external: true
  mattermost:
    name: "mattermost"

volumes:
  mattermost-config:
    name: mattermost-config
  mattermost-data:
    name: mattermost-data
  mattermost-plugins:
    name: mattermost-plugins
  mattermost-db:
    name: mattermost-db

services:
  mattermost:
    image: registry.picasoft.net/mattermost:9.11.6
    build:
      context: .
      args:
        MM_VERSION: 9.11.6
    container_name: mattermost-app
    security_opt:
      - no-new-privileges:true
    volumes:
      - mattermost-data:/mattermost/data
      - mattermost-plugins:/mattermost/plugins
      - /etc/localtime:/etc/localtime:ro
    env_file: ./secrets/mattermost-db.secrets
    labels:
      traefik.http.routers.mattermost-app.entrypoints: websecure
      traefik.http.routers.mattermost-app.rule: Host(`team.picasoft.net`)
      # See https://gitlab.utc.fr/picasoft/projets/services/traefik/-/blob/master/dynamic_config/traefik_dynamic.yaml
      traefik.http.routers.mattermost-app.middlewares: mattermost@file
      traefik.http.services.mattermost-app.loadbalancer.server.port: 8065
      traefik.enable: true
    networks:
      - proxy
      - mattermost
    depends_on:
      - mattermost-db
    ports:
      - 8443:8443/udp
      - 8443:8443/tcp
    restart: unless-stopped

  mattermost-db:
    image: postgres:15-alpine
    container_name: mattermost-db
    volumes:
      - mattermost-db:/var/lib/postgresql/data
      - /etc/localtime:/etc/localtime:ro
    env_file: ./secrets/mattermost-db.secrets
    networks:
      - mattermost
    restart: unless-stopped

  mattermost-exporter:
    image: registry.picasoft.net/pica-mattermost-exporter:0.1.0
    build: ./prometheus-exporter
    container_name: mattermost-exporter
    volumes:
      - /etc/localtime:/etc/localtime:ro
    env_file: ./secrets/mattermost-exporter.secrets
    networks:
      - mattermost
      - proxy
    labels:
      traefik.http.routers.mattermost-metrics.entrypoints: websecure
      traefik.http.routers.mattermost-metrics.rule: "Host(`team.picasoft.net`) && PathPrefix(`/metrics`)"
      traefik.http.routers.mattermost-metrics.service: mattermost-metrics
      traefik.http.routers.mattermost-metrics.middlewares: "mattermost-metrics-auth@docker"
      traefik.http.middlewares.mattermost-metrics-auth.basicauth.users: "${METRICS_AUTH}"
      traefik.http.services.mattermost-metrics.loadbalancer.server.port: 8000
      traefik.enable: true
    restart: unless-stopped

  mattermost-deactivate-bot:
    image: registry.picasoft.net/mattermost-deactivate-bot:0.1.0
    container_name: mattermost-deactivate-bot
    build:
      context: ./deactivate-bot
      args:
        GOMPLATE_VERSION: v3.11.7
        SUPERCRONIC_VERSION: v0.2.29
    env_file: ./secrets/deactivate-bot.secrets
    networks:
      - mattermost
    restart: unless-stopped
