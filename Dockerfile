# See https://docs.docker.com/reference/dockerfile/#understand-how-arg-and-from-interact
ARG MM_VERSION
# See https://team.picasoft.net/picasoft/pl/emi5ya98htbb7keu7ad8pnm86w
# Before, Mattermost Team was the same as Mattermost Enterprise
# but without a licence. Now, the official team image is built
# from the team archive, so we can use it as is.
FROM mattermost/mattermost-team-edition:${MM_VERSION}

ARG MM_VERSION
ADD --chmod=555 https://packages.framasoft.org/projects/mostlymatter/mostlymatter-v${MM_VERSION} /mattermost/bin/mattermost